<?php
use Sharecoto\JCalendar\Collection\Month;
use Sharecoto\JCalendar\Event;

class CollectionMonthTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->month = new Month(2014, 6);
    }

    public function testInstance()
    {
        $this->assertInstanceOf('Sharecoto\JCalendar\Collection\Collection', $this->month);
    }

    public function testNext()
    {
        $next = $this->month->getNext();
        $this->assertInstanceOf('Sharecoto\JCalendar\Collection\Month', $next);
        $this->assertEquals(7, $next->month);
    }

    public function testPrev()
    {
        $prev = $this->month->getPrev();
        $this->assertInstanceOf('Sharecoto\JCalendar\Collection\Month', $prev);
        $this->assertEquals(5, $prev->month);
    }

    public function testAddEvent()
    {
        $events = array();
        foreach(range(20,30) as $day) {
            $date = sprintf('2014-6-%d', $day);
            $param = array(
                'title' => 'test event ' . $date,
                'venue' => 'test venue',
                'dateFrom' => new DateTime($date)
            );
            $events[] = Event::createFromArray($param);
        }

        $this->month->addEvents($events);
    }

    public function testSetHolidays()
    {
        $holidays = array(
            array(
                'name' => 'テスト休日',
                'date' => '2014-6-29'
            ),
            array(
                'name' => 'テスト休日2',
                'date' => '2014-6-30'
            ),
        );
        $this->month->setHolidays($holidays);

        $day = $this->month->getDay(30);
        $this->assertInstanceOf('Sharecoto\JCalendar\Holiday', $day->holiday);
    }
}
