<?php

use \Sharecoto\JCalendar\Holiday;

class HolidayTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->data = array(
            'name' => 'テスト用の休日',
            'date' => new DateTime(
                '2014-5-5',
                new DateTimeZone('Asia/Tokyo')
            )
        );
    }

    public function testInstance()
    {
        $hday = new Holiday(
            $this->data['name'],
            $this->data['date']
        );

        $this->assertEquals($hday->name, $this->data['name']);
        $this->assertEquals($hday->date, $this->data['date']);

        $hday = new Holiday(
            $this->data['name'],
            $this->data['date']->format('Y-m-d')
        );
        $this->assertEquals($hday->date->format('Y-m-d'), $this->data['date']->format('Y-m-d'));
    }

    public function testCreateFromJson()
    {
        $data = array(
            'name' => $this->data['name'],
            'date' => $this->data['date']->format('Y-m-d')
        );
        $json = json_encode($data);
        $hday = Holiday::createFromJson($json);
        $this->assertInstanceOf('\Sharecoto\JCalendar\Holiday', $hday);
    }

}
