<?php
/**
 * Twigのお勉強用
 */

class TestTwig extends PHPUnit_Framework_TestCase
{
    public function testInstance()
    {
        $loader = new Twig_Loader_Filesystem(realpath(__DIR__ . '/../_files'));
        $twig = new Twig_Environment($loader, array(
            'cache' => '/tmp/test_twig'
        ));
        $template = 'test.twig';
        $renderd = $twig->render($template);
    }
}
