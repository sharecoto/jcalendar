<?php

use \Sharecoto\JCalendar\Event;

class EventTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->title = $title = 'This is Title of Event';
        $venue = 'Here is Venue of Event';

        $from = new DateTime('2001-1-1');
        $to = new DateTime('2001-1-5');

        $this->event = new Event($title, $venue, $from, $to);
    }

    public function testToString()
    {
        $this->assertEquals($this->title, $this->event->__toString());
    }

    public function testPeriod()
    {
        $date = new DateTime(date('Y-m-d'));
        $this->assertFalse($this->event->isInPeriod($date));

        $date =  new DateTime('2001-1-1');
        $this->assertTrue($this->event->isInPeriod($date));

        $date =  new DateTime('2001-1-5');
        $this->assertTrue($this->event->isInPeriod($date));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidTo()
    {
        $to = new DateTime('1999-1-1');
        $from = new DateTime('2000-1-1');

        $event = new Event('title', 'venue', $from, $to);
    }

    public function testIsSeries()
    {
        // 1日イベント
        $oneDayEvent = new Event(
            'title',
            'event',
            new DateTime('2014-01-01 6:00:00'),
            new DateTime('2014-01-01 12:00:00')
        );
        $this->assertFalse($oneDayEvent->isSeries());

        // 連日イベント
        $seriesEvent = new Event(
            'series',
            'event',
            new DateTime('2014-01-01 6:00:00'),
            new DateTime('2014-01-10 6:00:00')
        );
        $this->assertTrue($seriesEvent->isSeries());
    }
}
