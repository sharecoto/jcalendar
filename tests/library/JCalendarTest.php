<?php
class JCalendarTest extends PHPUnit_Framework_TestCase
{
    public function testInstance()
    {
        $calendar = new Sharecoto\JCalendar();

        $this->assertInstanceOf('DateTimeZone', $calendar->timezone);

        $cal = new Sharecoto\JCalendar(2015, 'Asia/Tokyo');
        $this->assertEquals(2015, $cal->year);

    }

    public function testGetMonth()
    {
        $cal = new \Sharecoto\JCalendar(2014);

        $month = $cal->getMonth(2);
        $this->assertInstanceOf('\Sharecoto\JCalendar\Collection\Month', $month);
    }

}
