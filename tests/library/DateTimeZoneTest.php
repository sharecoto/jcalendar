<?php

class DateTimeZoneTest extends PHPUnit_Framework_TestCase
{
    public function testTimeZoneInstance()
    {
        $tz = 'Asia/Tokyo';
        $timezone = new DateTimeZone($tz);
    }

    public function testDateTime()
    {
        $days = range(1, 31);
        $month = 0;
        $year = date('Y');
        $timezone = new DateTimeZone('Asia/Tokyo');

        foreach ($days as $day) {
            $dateString = sprintf('%d-%d-%d', $year, $month, $day);
            $date = new DateTime($dateString);
        }
    }

}
