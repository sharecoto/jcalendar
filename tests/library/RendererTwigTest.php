<?php

use \Sharecoto\JCalendar\Renderer\Twig;
use \Sharecoto\JCalendar\Renderer\DefaultRenderer;
use \Sharecoto\JCalendar\Collection\Month;
use \Sharecoto\JCalendar\Event;
use \Sharecoto\JCalendar\Collection\Collection;

class RendererTwigTest extends PHPUnit_Framework_TestCase
{
    public function testInstance()
    {
        $calendar = new Month();
        $renderer = new Twig($calendar);
        $this->assertInstanceOf('Twig_Environment', $renderer->getParser());
    }

    public function testRenderDefaultTemplate()
    {
        $year = 1970;
        $month = 7;
        $calendar = new Month($year, $month);
        $events = array(
            new Event(
                '藤井爆誕',
                '山梨県甲府市',
                new DateTime('1970-7-19')
            ),
        );
        $calendar->addEvents($events);

        $renderer = new Twig($calendar);

        $renderd = $renderer->render();
        $calendar = $renderer->getCalendar();

        file_put_contents('/tmp/calendar.html', $renderd);
    }

    public function testDefaultRendererExtendsTwig()
    {
        $year = 1970;
        $month = 7;
        $calendar = new Month($year, $month);

        $renderer = new DefaultRenderer($calendar);

        $renderd = $renderer->render();
        $calendar = $renderer->getCalendar();

        file_put_contents('/tmp/calendar_default.html', $renderd);
    }
}
