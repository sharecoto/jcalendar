<?php
use \Sharecoto\JCalendar\Collection\Day;
use \Sharecoto\JCalendar\Event;
use \Sharecoto\JCalendar\Holiday;

class DayTest extends PHPUnit_Framework_TestCase
{
    public function testInstance()
    {
        $day = new Day(
            2013,
            1,
            1
        );
        $this->assertEquals($day->year, 2013);
        $this->assertEquals($day->month, 1);
        $this->assertEquals($day->day, 1);
        $this->assertInstanceOf('DateTime', $day->date);
        $this->assertInstanceOf('DateTimeZone', $day->timezone);
        $this->assertFalse($day->isHoliday());
    }

    public function testSetHoliday()
    {
        $day = new Day(
            2013,
            1,
            1
        );

        $day->setHoliday('元旦');

        $this->assertInstanceOf('\Sharecoto\JCalendar\Holiday', $day->holiday);
        $this->assertTrue($day->isHoliday());
    }

    // イベントを一個づつ追加
    public function testAddEvent()
    {
        // 一日だけのイベント
        $event = new Event('title', 'venue', new DateTime(date('2013-1-1')));
        $day = new Day(
            2013,
            1,
            1
        );
        $day->addEvent($event);
        $this->assertTrue(in_array($event, $day->events));

        // 複数日にまたがるイベント
        $event = new Event('複数日イベント', 'venue', new DateTime('2012-12-30'), new DateTime('2013-1-5'));
        $day->addEvent($event);
        $this->assertTrue(in_array($event, $day->events));

        // イベント期間外の時は追加しない(例外スローしたほうがいいかな？　迷い中)
        $event = new Event('期間外', 'venue', new DateTime('2014-1-1'));
        $day->addEvent($event);
        $this->assertFalse(in_array($event, $day->events));

        // 配列からイベントを生成して追加
        $day->addEvent(
            array(
                'title' => '配列から',
                'venue' => 'venue',
                'dateFrom' => new DateTime('2013-1-1'),
                //'dateTo' => new DateTime('2013-1-10'),
                'options' => array(
                    'location' => '住所詳細'
                )
            )
        );
        $events = $day->events;
        $lastEvent = array_pop($events);
        $this->assertEquals('配列から', $lastEvent->title);
    }
}
