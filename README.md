# イベントカレンダーを作れるクラス

たぶん日本国内でしか使えないっぽいカレンダークラスです。

## できること。

* 月別のカレンダー生成
* 休日を設定できる
* イベントを設定できる

## Requirs

* twig/twig: >=1.15.1
* php: >= 5.3

## Composerでインストール

packagist.orgに登録してないので、以下のような感じで``composer.json``にリポジトリ情報を書く必要があります。

``composer.json``を編集したら、``$ composer.phar install``でOKです。

```
{
"repositories": [
    {
    "type": "vcs",
    "url": "git@bitbucket.org:sharecoto/jcalendar.git"
    }
  ],
  "require": {
    "sharecoto/jcalendar": "dev-master"
  }
}
```

## 使い方。

ざっくり。

### 当年1月のカレンダーを生成。
```php
$calendar = new Sharecoto\JCalendar();
$month = $calendar->getMonth(1); // Sharecoto\JCalendar\Collection\Monthクラス。
```

### 年を指定する。
```php
$calendar = new Sharecoto\JCalendar(1970);
$month = $calendar->getMonth(7);
```

### 生成した月別カレンダーから前後の月を取得
```php
$prevMonth = $month->getPrev();
$nextMonth = $month->getNext();
```

月カレンダーから個別の日付を取得する
```php
$month->getDay(1); //1日を取得
```

### 休日を設定する

休日データは内閣府の[「国民の祝日」について](http://www8.cao.go.jp/chosei/shukujitsu/gaiyou.html)なんかを参照して手動で作成する必要があります。

```php
$calendar = new Sharecoto\JCalendar(2014);
$month = $calendar->getMonth(5);
$holidays = array(
    array (
        'name' => '憲法記念日',
        'date' => new DateTime('2014-5-3'),
    ),
    array (
        'name' => 'みどりの日',
        'date' => new DateTime('2014-5-4'),
    ),
    array (
        'name' => 'こどもの日',
        'date' => new DateTime('2014-5-5'),
    ),
    array (
        'name' => '振替休日',
        'date' => new DateTime('2014-5-6'),
    ),
);
$month->setHolidays($holidays)
```

### イベントを設定する
```php
$calendar = new Sharecoto\JCalendar(1970);
$month = $calendar->getMonth(7);

$events = array (
    new Sharecoto\JCalendar\Event(
        '藤井誕生日', //イベント名
        '山梨県甲府市', // 場所
        new DateTime('1970-7-19'), // イベント開始日
    ),
);

$month->addEvents($events);
```

### 表示する
```php
$renderer = new Sharecoto\JCalendar\Renderer\Twig($month);
$renderer->dump();

// レンダリング結果を取得
$html = $renderer->render();
```

## TODO
* 週単位で生成
* 期間を指定して生成
* 繰り返しイベント対応(クラス側で対応する必要があるかどうかは要検討)
* Twig以外のテンプレートエンジンを使えるようにするかもしれない
* 設計がヨレヨレなので、もうちょっと考えよう
* GoogleカレンダーAPI経由で祝日自動生成