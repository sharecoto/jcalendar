<?php

namespace Sharecoto\JCalendar;

use \DateTime;
use \DateTimeZone;

class Event
{
    /**
     * イベント開始日時
     * 必須
     *
     * @var DateTime
     */
    public $dateFrom;

    /**
     * イベント終了日時
     * 必須
     *
     * @var DateTime
     */
    public $dateTo;

    /**
     * イベント名
     * 必須
     *
     * @var string
     */
    public $title;

    /**
     * 概要
     *
     * @var string
     */
    public $description;

    /**
     * 会場名
     * 必須
     *
     * @var string
     */
    public $venue;

    /**
     * 住所
     */
    public $location;

    /**
     * 主催者
     *
     * @var string
     */
    public $organizer;

    public function __construct($title, $venue, DateTime $from, DateTime $to = null, $options = array())
    {
        //タイトルセット
        if (!is_string($title)) {
            throw new \InvalidArgumentException('$title only accepts strings');
        }
        $this->title = $title;

        // 地名（会場名）セット
        if (!is_string($venue)) {
            throw new \InvalidArgumentException('$venue only accepts strings');
        }
        $this->venue = $venue;

        // 期間セット
        $this->dateFrom = $from;
        if (!$to) {
            // fromの正規化(時刻を00:00:00にする)
            $date = new DateTime($from->format('Y-m-d'));
            $this->dateTo = $date;
            $this->dateFrom = $date;
        } else {
            // toバリデーション
            // fromより過去ではいけない
            if ($from > $to) {
                throw new \InvalidArgumentException('$to is less than $from');
            }
            $this->dateTo = $to;
        }

        foreach ($options as $key=>$value) {
            $this->$key = $value;
        }
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @param DateTime|string $date
     * @param bool $onlyDate // 日付だけの比較をするかどうか
     * @return boolean
     */
    public function isInPeriod($date, $onlyDate = true)
    {
        $from = $this->dateFrom;
        $to = $this->dateTo;
        if ($onlyDate) {
            $from = new DateTime($this->dateFrom->format('Y-m-d'));
            $to = new DateTime($this->dateTo->format('Y-m-d'));
            $date = new DateTime($date->format('Y-m-d'));
        }

        if ($date >= $from && $date <= $to) {
            return true;
        }

        return false;
    }

    /**
     * 配列からインスタンスを生成
     * $eventの配列の形式
     * array (
     *  'title' => 'title', // required
     *  'venue' => 'vanue', // required
     *  'dateFrom' => DateTime, // required
     *  'dateTo' => DateTime, // option
     *  'options' => array(
     *  ) // option
     * )
     *
     * @param array $event
     */
    public static function createFromArray($event)
    {
        $event = new self(
            $event['title'],
            $event['venue'],
            $event['dateFrom'],
            isset($event['dateTo']) ? $event['dateTo'] : null,
            isset($event['options']) ? $event['options'] : array()
        );
        return $event;
    }

    /**
     * 連日イベントならtrue
     * 1日イベントならfalse
     *
     * @return bool
     */
    public function isSeries()
    {
        $fromDate = $this->dateFrom->format('Y-m-d');
        $toDate = $this->dateTo->format('Y-m-d');

        if ($fromDate == $toDate) {
            return false;
        }
        return true;
    }
}
