<?php
namespace Sharecoto\JCalendar;

use \DateTime;
use \DateTimeZone;

class Holiday
{
    private $name;
    private $date;
    private $timezone;
    private $timezoneDefault = 'Asia/Tokyo';

    /**
     * @param string $name
     * @param string|DateTime $date
     * @param bool $substitute 振替休日か?
     */
    public function __construct($name, $date, $timezone=null)
    {
        $this->name = $name;

        if ($timezone instanceof DateTimeZone) {
            $this->timezone = $timezone;
        } else {
            if (!$timezone) {
                $timezone = $this->timezoneDefault;
            }
            $this->timezone = new DateTimeZone($timezone);
        }

        if ($date instanceof DateTime) {
            $this->date = $date;
        } else {
            $this->date = new DateTime($date, $this->timezone);
        }

    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }

        throw new \ErrorException('Undefined valiables');
    }

    /**
     * JSONデータから新しいインスタンスを返す
     *
     * @param string $json
     */
    public static function createFromJson($json)
    {
        $data = json_decode($json);

        return self::createFromArray((array)$data);
    }

    /**
     * 配列から新しいインスタンスを返す
     */
    public static function createFromArray($data)
    {
        return new self(
            $data['name'],
            $data['date'],
            isset($data['timezone']) ? $data['timezone'] : null
        );
    }
}
