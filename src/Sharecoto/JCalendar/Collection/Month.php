<?php
namespace Sharecoto\JCalendar\Collection;

use \DateTime;
use \DateTimeZone;

class Month extends Collection
{
    public function __construct($year = null, $month=null, $timezone=null, $events=array())
    {
        if (!(integer)$month) {
            $month = date('n');
        }
        $this->month = $month;

        if ($year === null) {
            $year = date('Y');
        }
        if (!is_numeric($year)) {
            throw new \InvalidArgumentException('First Argument is Invalid.');
        }
        $this->year = (integer)$year;

        if ($timezone instanceof DateTimeZone) {
            $this->timezone = $timezone;
        } elseif ($timezone) {
            $this->timezone = new DateTimeZone($timezone);
        } else {
            $this->timezone = new DateTimeZone('Asia/Tokyo');
        }

        $this->setDays();
        if (count($events)) {
            $this->addEvents($events);
        }

        parent::__construct();
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
        throw new \OutOfBoundsException();
    }

    public function __isset($name)
    {
        if (isset($this->$name)) {
            return true;
        }
        return false;
    }

    public function setDays()
    {
        $days = range(1, 31);
        foreach ($days as $day) {
            $dayString = sprintf('%d-%d-%d', $this->year, $this->month, $day);
            $date = new Day($this->year, $this->month, $day, $this->timezone);
            if ((integer)$date->date->format('n') !== (integer)$this->month) {
                break;
            }
            $this->append($date);
        }
        return $this;
    }

    public function getNext()
    {
        if ($this->month == 12) {
            $year = $this->year + 1;
            return new self(
                $year,
                1,
                $this->timezone
            );
        }

        $month = $this->month + 1;
        return new self(
            $this->year,
            $month,
            $this->timezone
        );
    }

    public function getPrev()
    {
        if ($this->month == 1) {
            $year = $this->year - 1;
            return new self(
                $year,
                12,
                $this->timezone
            );
        }

        $month = $this->month - 1;
        return new self(
            $this->year,
            $month,
            $this->timezone
        );
    }

    /**
     * カレンダーにイベントをまとめて追加する
     *
     * @param array $events イベントのインスタンスを含む配列
     */
    public function addEvents(array $events)
    {
        foreach ($events as $event) {
            if (!($event instanceof \Sharecoto\JCalendar\Event)) {
                // 例外投げるべきかも。迷い中
                continue;
            }
            foreach ($this as $day) {
                $day->addEvent($event);
            }
        }
        return $this;
    }

    /**
     * カレンダーに休日をまとめて追加する
     *
     * $holidays = array(
     *  array(
     *      'name' => 'holiday name',
     *      'date' => DateTime | date
     *  )
     * );
     *
     * @param array $holydays
     */
    public function setHolidays(array $holidays)
    {
        foreach ($holidays as $holiday) {
            if (!($holiday['date'] instanceof \DateTime)) {
                $holiday['date'] = new \DateTime($holiday['date']);
            }

            foreach ($this as $day) {
                if ($day->date == $holiday['date']) {
                    $day->setHoliday($holiday['name']);
                    break;
                }
            }
        }
        return $this;
    }

    public function getDay($day)
    {
        foreach ($this as $k=>$date) {
            if ($day == $date->day) {
                return $date;
            }
        }
        return false;
    }
}

