<?php
namespace Sharecoto\JCalendar\Collection;

use \DateTime;
use \DateTimeZone;
use \Sharecoto\JCalendar\Event;
use \Sharecoto\JCalendar\Holiday;

class Day
{
    private $year;
    private $month;
    private $day;
    private $timezone;

    /**
     * @var \DateTime;
     */
    private $date;

    /**
     * @var null|Holiday
     */
    private $holiday;

    /**
     * @var array
     */
    private $events;

    public function __construct($year = null, $month = null, $day = null, $timezone=null)
    {
        if ($year === null) {
            $year = date('y');
        }
        if (!is_numeric($year)) {
            throw new \InvalidArgumentException('First Argument is Invalid.');
        }
        $this->year = (integer)$year;

        if ((integer)$month === 0) {
            $month = date('n');
        }
        $this->month = (integer)$month;

        if ((integer)$day === 0) {
            $day = date('j');
        }
        $this->day = $day;

        if ($timezone instanceof DateTimeZone) {
            $this->timezone = $timezone;
        } elseif ($timezone) {
            $this->timezone = new DateTimeZone($timezone);
        } else {
            $this->timezone = new DateTimeZone('Asia/Tokyo');
        }

        $this->date = new DateTime(
            sprintf('%d-%d-%d', $this->year, $this->month, $this->day),
            $this->timezone
        );
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
        throw new \OutOfBoundsException();
    }

    /**
     * twigでisset()しているようなので
     */
    public function __isset($name)
    {
        if (isset($this->$name)) {
            return true;
        }
        return false;
    }

    /**
     * 日付にイベントをセットする
     * $eventの配列の形式
     * array (
     *  'title' => 'title', // required
     *  'venue' => 'vanue', // required
     *  'dateFrom' => DateTime, // required
     *  'dateTo' => DateTime, // option
     *  'options' => array(
     *  ) // option
     * )
     *
     * @param \Sharecoto\JCalendar\Event | array $event
     */
    public function addEvent($event)
    {
        if (is_array($event)) {
            $event = Event::createFromArray($event);
        }

        if (!$this->events) {
            $this->events = array();
        }

        // バリデーションイベントの日付確認
        // 例外吐いたほうがいいかな？
        // 現状は単に無視します
        if ($event->isInPeriod($this->date)) {
            $this->events[] = $event;
        }

        return $this;
    }

    public function hasEvent()
    {
        if (count($this->events)) {
            return true;
        }
        return false;
    }

    /**
     * 休日フラグをセットする
     *
     * @param string $holidayName
     */
    public function setHoliday($holidayName)
    {
        $this->holiday = new Holiday(
            $holidayName,
            $this->date
        );
        return $this;
    }

    /**
     * 休日かどうかを返す
     *
     * @return bool
     */
    public function isHoliday()
    {
        if ($this->date->format('w') == 0) {
            return true;
        }
        return (bool) $this->holiday;
    }
}
