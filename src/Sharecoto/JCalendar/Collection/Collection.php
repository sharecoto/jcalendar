<?php
namespace Sharecoto\JCalendar\Collection;

class Collection extends \ArrayObject
{
    protected $year;
    protected $month;
    protected $timezone;
}
