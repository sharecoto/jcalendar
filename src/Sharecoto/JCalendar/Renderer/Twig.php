<?php
namespace Sharecoto\JCalendar\Renderer;

use \Sharecoto\JCalender\Collection\Collection;

use \Twig_Autoloader;
use \Twig_Loader_Filesystem;
use \Twig_Environment;

class Twig extends Renderer
{
    /**
     * $twigOptions = array(
     *  'templates' => 'path/to/templates',
     *  'environment = array(
     *      'cache' => 'path/to/cache_dir',
     *  )
     * );
     */
    public function __construct($calendar, $template=null, Twig_Environment $parser=null, array $twigOptions=array())
    {
        if (!$parser) {
            $parser = $this->getTwig($twigOptions);
        }

        parent::__construct($calendar, $template, $parser);
    }

    /**
     * @return Twig_Environment
     */
    public function getTwig(array $options)
    {
        if ($this->parser instanceof Twig_Environment) {
            return $this->parser;
        }

        Twig_Autoloader::register();
        if (isset($options['templates'])) {
            $dir = $options['templates'];
        } else {
            // デフォルトのtemplatespath
            $dir = __DIR__ . '/templates';
        }
        $loader = new Twig_Loader_Filesystem($dir);

        $environment = array();
        if (isset($options['environment'])) {
            $environment = $options['environment'];
        }

        $twig = new Twig_Environment($loader, $environment);
        return $twig;
    }

    public function setParser($parser)
    {
        if ($parser instanceof Twig_Environment) {
            $this->parser = $parser;
            return $this;
        }

        throw new Exception('$parser requires Twig_Environment');
    }

    public function dump($template = null, $data = array())
    {
        echo $this->render($template, $data);
    }

    public function render($template = null, $data = array())
    {
        if (!$template) {
            $template = 'default.twig';
        }

        $data = array_merge(
            $data,
            array (
                'calendar' => $this->calendar
            )
        );

        return $this->parser->render(
            $template,
            $data
        );
    }
}
