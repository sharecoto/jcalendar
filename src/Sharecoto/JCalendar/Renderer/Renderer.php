<?php
/**
 * カレンダー描画の抽象クラス
 *
 * @author fujii@sharecoto.co.jp
 */

namespace Sharecoto\JCalendar\Renderer;

use \Sharecoto\JCalender\Collection\Collection;

abstract class Renderer
{
    /**
     * テンプレートディレクトリ
     * @var string
     */
    protected $templates;

    /**
     * デフォルトのテンプレートディレクトリ
     *
     * @var string
     */
    protected $defaultTemplates;

    /**
     * テンプレートエンジン
     */
    protected $parser;

    /**
     * カレンダーデータ
     *
     * @var Sharecoto\JCalender\Collection\Collection $calendar
     */
    protected $calendar;

    /**
     *
     * @param Sharecoto\JCalender\Collection\Collection $calendar
     */
    public function __construct($calendar, $templates=null, $parser=null)
    {
        if (!($calendar instanceof \Sharecoto\JCalendar\Collection\Collection)) {
            throw new Exception('$calendar requires instance of Sharecoto\JCalender\Collection\Collection');
        }
        $this->setCalendar($calendar);

        $this->defaultTemplates = realpath(__DIR__ . '/templates/');
        if (!$templates) {
            $templates = realpath($this->defaultTemplates);
        }
        $this->setTemplates($templates);
        $this->setParser($parser);
    }

    abstract public function dump($template);

    abstract public function render($template = null);

    abstract public function setParser($parser);

    public function setCalendar($calendar)
    {
        $this->calendar = $calendar;
        return $this;
    }

    public function getCalendar()
    {
        return $this->calendar;
    }

    public function getParser() {
        return $this->parser;
    }

    public function setTemplates($path) {
        $this->template = $path;
    }

    public function getTemplateDirs() {
        return $this->template;
    }

    public function __toString()
    {
        return $this->render();
    }
}
