<?php
namespace Sharecoto\JCalendar;
use \DateTime;
use \DateTimeZone;

class Month
{
    private $month;
    private $year;
    private $timezone;

    /**
     * @var Sharecoto\JCalendar\Collection\Month
     */
    private $calendar;

    public function __construct($year = null, $month=null, $timezone=null)
    {
        if (!(integer)$month) {
            $month = date('n');
        }
        $this->month = $month;

        if ($year === null) {
            $year = date('Y');
        }
        if (!is_numeric($year)) {
            throw new \InvalidArgumentException('First Argument is Invalid.');
        }
        $this->year = (integer)$year;

        if ($timezone instanceof DateTimeZone) {
            $this->timezone = $timezone;
        } elseif ($timezone) {
            $this->timezone = new DateTimeZone($timezone);
        } else {
            $this->timezone = new DateTimeZone('Asia/Tokyo');
        }

        $this->getCalendar();
    }

    public function getNext()
    {
        if ($this->month == 12) {
            $year = $this->year + 1;

            return new self(
                $year,
                1,
                $this->timezone
            );
        }

        $month = $this->month + 1;
        return new self(
            $this->year,
            $month,
            $this->timezone
        );
    }

    public function getPrev()
    {
        if ($this->month == 1) {
            $year = $this->year - 1;
            return new self(
                $year,
                12,
                $this->timezone
            );
        }

        $month = $this->month - 1;
        return new self(
            $this->year,
            $month,
            $this->timezone
        );
    }

    /**
     * 日付を含む配列を返す
     *
     * @return Sharecoto\JCalendar\Collection\Collection
     */
    public function getCalendar()
    {
        if ($this->calendar) {
            return $this->calendar;
        }

        $calendar = new Collection\Month;
        $days = range(1, 31);
        foreach ($days as $day) {
            $dayString = sprintf('%d-%d-%d', $this->year, $this->month, $day);
            $date = new Day($this->year, $this->month, $day, $this->timezone);
            if ((integer)$date->date->format('n') !== (integer)$this->month) {
                break;
            }
            $calendar->append($date);
        }

        $this->calendar = $calendar;
        return $this->calendar;
    }

    /**
     * カレンダーにイベントをまとめて追加する
     *
     * @param array $events イベントのインスタンスを含む配列
     */
    public function addEvents(array $events)
    {
        if (!$this->calendar) {
            $this->getCalendar();
        }
        foreach ($events as $event) {
            if (!($event instanceof \Sharecoto\JCalendar\Event)) {
                // 例外投げるべきかも。迷い中
                continue;
            }
            foreach ($this->calendar as $day) {
                $day->addEvent($event);
            }
        }
        return $this;
    }

    /**
     * 一日だけ取り出す
     *
     * @param integer $day
     * @return \Sharecoto\JCalendar\Day | false
     */
    public function getDay($day)
    {
        return $this->calendar->getDay($day);
    }
}
