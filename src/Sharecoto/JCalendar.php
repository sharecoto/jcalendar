<?php
namespace Sharecoto;

use \DateTime;
use \DateTimeZone;

class JCalendar
{
    private $year;
    private $timezone;

    public function __construct($year = null, $timezone = null)
    {
        if (!$year) {
            $year = date('Y');
        }
        $this->year = $year;

        if ($timezone) {
            if ($timezone instanceof DateTime) {
            } else {
                $timezone = new DateTimeZone($timezone);
            }
        } else {
            $timezone = new DateTimeZone('Asia/Tokyo');
        }
        $this->timezone = $timezone;
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
        throw new \OutOfBoundsException();
    }

    public function getMonth($month = null)
    {
        $month = new JCalendar\Collection\Month($this->year, $month, $this->timezone);
        return $month;
    }
}
